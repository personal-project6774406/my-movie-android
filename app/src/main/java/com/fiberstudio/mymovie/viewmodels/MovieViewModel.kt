package com.fiberstudio.mymovie.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.fiberstudio.mymovie.models.movies.detail.DetailResponse
import com.fiberstudio.mymovie.models.movies.reviews.ReviewResponse
import com.fiberstudio.mymovie.models.genres.Response as Genres
import com.fiberstudio.mymovie.models.movies.list.Response as Movies
import com.fiberstudio.mymovie.repositories.MovieRepository
import com.fiberstudio.mymovie.utils.networking.RequestState
import kotlinx.coroutines.launch
import org.json.JSONException
import org.json.JSONObject
import retrofit2.HttpException
import retrofit2.Response

class MovieViewModel: ViewModel() {
    private val repo: MovieRepository = MovieRepository()
    private var discoverMoviePage = 1
    private var reviewPage = 1
    private var discoverMovieResponse: Movies? = null
    private var moviesReviewResponse: ReviewResponse? = null
    private var _discoverResponse = MutableLiveData<RequestState<Movies?>>()
    private var _reviewResponse = MutableLiveData<RequestState<ReviewResponse?>>()
    var discoverResponse: LiveData<RequestState<Movies?>> = _discoverResponse
    var reviewResponse: LiveData<RequestState<ReviewResponse?>> = _reviewResponse

    fun getGenres(): LiveData<RequestState<Genres>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repo.getGenres()
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            e.response()?.errorBody()?.string()?.let { RequestState.Error(it) }?.let { emit(it) }
        }
    }

    fun discoverMovie(genreId: String) {
        viewModelScope.launch {
            _discoverResponse.postValue(RequestState.Loading)
            val response = repo.discoverMovies(discoverMoviePage, genreId)
            _discoverResponse.postValue(handleDiscoverMoviesMovieResponse(response))
        }
    }

    private fun handleDiscoverMoviesMovieResponse(response: Response<Movies>): RequestState<Movies?> {
        return if (response.isSuccessful) {
            response.body()?.let {
                discoverMoviePage++
                if (discoverMovieResponse == null) discoverMovieResponse = it else {
                    val oldMovies = discoverMovieResponse?.results
                    val newMovies = it.results
                    oldMovies?.addAll(newMovies)
                }
            }

            RequestState.Success(discoverMovieResponse ?: response.body())
        } else RequestState.Error(
            try {
                response.errorBody()?.string()?.let {
                    JSONObject(it).get("status_message")
                }
            } catch (e: JSONException) {
                e.localizedMessage
            } as String
        )
    }

    fun getDetailMovie(id: Int): LiveData<RequestState<DetailResponse>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repo.getDetailMovie(id)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            e.response()?.errorBody()?.string()?.let { RequestState.Error(it) }?.let { emit(it) }
        }
    }

    fun getMoviesReviews(id: Int) {
        viewModelScope.launch {
            _reviewResponse.postValue(RequestState.Loading)
            val response = repo.getMoviesReviews(id, reviewPage)
            _reviewResponse.postValue(handleReviewResponse(response))
        }
    }

    private fun handleReviewResponse(response: Response<ReviewResponse>): RequestState<ReviewResponse?> {
        return if (response.isSuccessful) {
            response.body()?.let {
                reviewPage++
                if (moviesReviewResponse == null) moviesReviewResponse = it else {
                    val oldReviews = moviesReviewResponse?.results
                    val newReviews = it.results
                    oldReviews?.addAll(newReviews)
                }
            }

            RequestState.Success(moviesReviewResponse ?: response.body())
        } else RequestState.Error(
            try {
                response.errorBody()?.string()?.let {
                    JSONObject(it).get("status_message")
                }
            } catch (e: JSONException) {
                e.localizedMessage
            } as String
        )
    }

    fun getUpcomingMovies(page: Int): LiveData<RequestState<Movies>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repo.getUpcomingMovies(page)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            e.response()?.errorBody()?.string()?.let { RequestState.Error(it) }?.let { emit(it) }
        }
    }

    fun getUpcomingMovieList() {
        viewModelScope.launch {
            _discoverResponse.postValue(RequestState.Loading)
            val response = repo.getUpcomingMovieList(discoverMoviePage)
            _discoverResponse.postValue(handleGetUpcomingMovieListResponse(response))
        }
    }

    private fun handleGetUpcomingMovieListResponse(response: Response<Movies>): RequestState<Movies?> {
        return if (response.isSuccessful) {
            response.body()?.let {
                discoverMoviePage++
                if (discoverMovieResponse == null) discoverMovieResponse = it else {
                    val oldMovies = discoverMovieResponse?.results
                    val newMovies = it.results
                    oldMovies?.addAll(newMovies)
                }
            }

            RequestState.Success(discoverMovieResponse ?: response.body())
        } else RequestState.Error(
            try {
                response.errorBody()?.string()?.let {
                    JSONObject(it).get("status_message")
                }
            } catch (e: JSONException) {
                e.localizedMessage
            } as String
        )
    }

    fun getPopularMovies(page: Int): LiveData<RequestState<Movies>> = liveData {
        emit(RequestState.Loading)
        try {
            val response = repo.getPopularMovies(page)
            emit(RequestState.Success(response))
        } catch (e: HttpException) {
            e.response()?.errorBody()?.string()?.let { RequestState.Error(it) }?.let { emit(it) }
        }
    }

    fun getPopularMovieList() {
        viewModelScope.launch {
            _discoverResponse.postValue(RequestState.Loading)
            val response = repo.getUpcomingMovieList(discoverMoviePage)
            _discoverResponse.postValue(handleGetUpcomingMovieListResponse(response))
        }
    }

    private fun handleGetPopularMovieListResponse(response: Response<Movies>): RequestState<Movies?> {
        return if (response.isSuccessful) {
            response.body()?.let {
                discoverMoviePage++
                if (discoverMovieResponse == null) discoverMovieResponse = it else {
                    val oldMovies = discoverMovieResponse?.results
                    val newMovies = it.results
                    oldMovies?.addAll(newMovies)
                }
            }

            RequestState.Success(discoverMovieResponse ?: response.body())
        } else RequestState.Error(
            try {
                response.errorBody()?.string()?.let {
                    JSONObject(it).get("status_message")
                }
            } catch (e: JSONException) {
                e.localizedMessage
            } as String
        )
    }
}