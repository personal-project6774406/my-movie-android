package com.fiberstudio.mymovie.repositories

import com.fiberstudio.mymovie.BuildConfig
import com.fiberstudio.mymovie.utils.networking.ApiConfig

class MovieRepository {
    private val client = ApiConfig.getApiService()
    suspend fun getGenres() = client.getGenres(BuildConfig.API_KEY)
    suspend fun discoverMovies(page: Int, genreId: String) = client.discoverMovies(BuildConfig.API_KEY, genreId, page)
    suspend fun getDetailMovie(id: Int) = client.getDetailMovie(id, BuildConfig.API_KEY, "videos")
    suspend fun getMoviesReviews(id: Int, page: Int) = client.getMoviesReviews(id, BuildConfig.API_KEY, page)
    suspend fun getUpcomingMovies(page: Int) = client.getUpcomingMovies(BuildConfig.API_KEY, page)
    suspend fun getUpcomingMovieList(page: Int) = client.getUpcomingMovieList(BuildConfig.API_KEY, page)
    suspend fun getPopularMovies(page: Int) = client.getPopularMovies(BuildConfig.API_KEY, page)
    suspend fun getPopularMovieList(page: Int) = client.getPopularMovieList(BuildConfig.API_KEY, page)
}