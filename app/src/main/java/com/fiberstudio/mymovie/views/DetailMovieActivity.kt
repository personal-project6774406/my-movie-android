package com.fiberstudio.mymovie.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebChromeClient
import android.webkit.WebViewClient
import androidx.activity.viewModels
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fiberstudio.mymovie.BuildConfig
import com.fiberstudio.mymovie.databinding.ActivityDetailMovieBinding
import com.fiberstudio.mymovie.utils.DateAndTimeUtils
import com.fiberstudio.mymovie.utils.adapters.ReviewsAdapter
import com.fiberstudio.mymovie.utils.dialogues.AlertDialog
import com.fiberstudio.mymovie.utils.dialogues.LoadingDialog
import com.fiberstudio.mymovie.utils.networking.RequestState
import com.fiberstudio.mymovie.viewmodels.MovieViewModel

class DetailMovieActivity : AppCompatActivity() {
    private var _binding: ActivityDetailMovieBinding? = null
    private val binding get() = _binding
    private var loading: LoadingDialog? = null
    private var alert: AlertDialog? = null
    private val viewModel: MovieViewModel by viewModels()
    private var adapter: ReviewsAdapter? = null
    private var movieId = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityDetailMovieBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        // setup dialogues
        loading = LoadingDialog(this)
        alert = AlertDialog(this)

        movieId = intent.getIntExtra("GENRES_ID",0)

        // setup youtube player (web view). youtube player API is now deprecated in android
        binding?.apply {
            webView.settings.javaScriptEnabled = true
            webView.webChromeClient = WebChromeClient()
            webView.webViewClient = WebViewClient()
        }

        getDetailMovie(movieId)
        viewModel.getMoviesReviews(movieId)
        observeAnychangeReviews()
        setupRecyclerView()
    }

    private fun getDetailMovie(id: Int) {
        val movieGenres = StringBuilder()
        viewModel.getDetailMovie(id).observe(this) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> loading?.show()
                    is RequestState.Success -> it.data.let { data ->
                        loading?.hide()
                        with(data) {
                            binding?.apply {
                                Glide.with(this@DetailMovieActivity).load("${BuildConfig.IMAGE_URL}${posterPath}").into(posterDetail)
                                titleDetail.text = title
                                if (genres != null) {
                                    for (genre in genres) movieGenres.append("${genre?.name}, ")
                                    genreDetail.text = movieGenres.dropLast(2)
                                }
                                releaseDateDetail.text = releaseDate?.let { date ->
                                    DateAndTimeUtils.formatDate(date)
                                }
                                ratingText.text = String.format("%.1f", voteAverage).toDouble().toString()
                                ratingBar.rating = voteAverage?.div(2) ?: 0f
                                overview.text = data.overview

                                // set the movie trailer
                                val videos = videos?.results?.filter { trailer -> trailer?.type?.contains("Trailer") == true }
                                val videoId = videos?.get(0)?.key //  get latest trailer
                                val html = "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/$videoId\" frameborder=\"0\" allowfullscreen></iframe>"
                                webView.loadData(html, "text/html", "utf-8")
                            }
                        }
                    }
                    is RequestState.Error -> {
                        loading?.hide()
                        alert?.errorAlert(it.message)
                    }
                }
            }
        }
    }

    private fun observeAnychangeReviews() {
        viewModel.reviewResponse.observe(this) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}
                    is RequestState.Success -> {
                        it.data?.results?.let { data ->
                            adapter?.differ?.submitList(data.toList())
                        }
                    }
                    is RequestState.Error -> alert?.errorAlert(it.message)
                }
            }
        }
    }

    private fun setupRecyclerView() {
        adapter = ReviewsAdapter()
        binding?.apply {
            rvReview.adapter = adapter
            rvReview.addOnScrollListener(scrollListener)
        }
    }

    private val scrollListener = object: RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            if (!recyclerView.canScrollVertically(1)) {
                viewModel.getMoviesReviews(movieId)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}