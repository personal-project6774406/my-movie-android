package com.fiberstudio.mymovie.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import com.fiberstudio.mymovie.utils.adapters.GenresAdapter
import com.fiberstudio.mymovie.databinding.ActivityGenresBinding
import com.fiberstudio.mymovie.utils.adapters.GridMovieAdapter
import com.fiberstudio.mymovie.utils.dialogues.AlertDialog
import com.fiberstudio.mymovie.utils.dialogues.LoadingDialog
import com.fiberstudio.mymovie.utils.listeners.OnGenreClickListener
import com.fiberstudio.mymovie.utils.listeners.OnMovieClickListener
import com.fiberstudio.mymovie.utils.networking.RequestState
import com.fiberstudio.mymovie.viewmodels.MovieViewModel

class GenresActivity : AppCompatActivity() {
    private var _binding: ActivityGenresBinding? = null
    private val binding get() = _binding
    private var adapter: GenresAdapter? = null
    private var moviesAdapter: GridMovieAdapter? = null
    private var popularMoviesAdapter: GridMovieAdapter? = null
    private val viewModel: MovieViewModel by viewModels()
    private var loading: LoadingDialog? = null
    private var alert: AlertDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityGenresBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        // setup dialogues
        loading = LoadingDialog(this)
        alert = AlertDialog(this)

        // setup adapter
        adapter = GenresAdapter(this)
        moviesAdapter = GridMovieAdapter()
        popularMoviesAdapter = GridMovieAdapter()

        getGenres()
        getUpcomingMovies()
        getPopularMovies()

        // handle on genre click
        adapter?.setOnGenreClickListener(object: OnGenreClickListener{
            override fun onGenreClick(id: Int, genres: String) {
                startActivity(Intent(this@GenresActivity, MoviesActivity::class.java)
                    .putExtra("GENRES_ID", id)
                    .putExtra("GENRES", genres)
                    .putExtra("STATE", 0)) // 0 means for discover movies by genre
            }
        })

        moviesAdapter?.setOnMovieClickListener(object: OnMovieClickListener {
            override fun onMovieClick(id: Int) {
                startActivity(Intent(this@GenresActivity, DetailMovieActivity::class.java).putExtra("GENRES_ID", id))
            }
        })

        popularMoviesAdapter?.setOnMovieClickListener(object: OnMovieClickListener {
            override fun onMovieClick(id: Int) {
                startActivity(Intent(this@GenresActivity, DetailMovieActivity::class.java).putExtra("GENRES_ID", id))
            }
        })

        binding?.more?.setOnClickListener {
            startActivity(Intent(this@GenresActivity, MoviesActivity::class.java)
                .putExtra("TITLE", "Upcoming Movies")
                .putExtra("STATE", 1)) // 1 means for discover upcoming movies
        }

        binding?.popularMore?.setOnClickListener {
            startActivity(Intent(this@GenresActivity, MoviesActivity::class.java)
                .putExtra("TITLE", "Popular Movies")
                .putExtra("STATE", 2)) // 1 means for discover popular movies
        }
    }

    private fun getGenres() {
        viewModel.getGenres().observe(this) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> loading?.show()
                    is RequestState.Success -> it.data.genres?.let { data ->
                        loading?.hide()
                        adapter?.setGenres(data)
                        moviesAdapter?.setGenres(data)
                        popularMoviesAdapter?.setGenres(data)
                        binding?.rvGenres?.adapter = adapter
                    }
                    is RequestState.Error -> {
                        loading?.hide()
                        alert?.errorAlert(it.message)
                    }
                }
            }
        }
    }

    private fun getUpcomingMovies() {
        viewModel.getUpcomingMovies(1).observe(this) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> loading?.show()
                    is RequestState.Success -> it.data.results.let { data ->
                        loading?.hide()
                        moviesAdapter?.setMovies(data)
                        binding?.rvUpcomingMovie?.adapter = moviesAdapter
                    }
                    is RequestState.Error -> {
                        loading?.hide()
                        alert?.errorAlert(it.message)
                    }
                }
            }
        }
    }

    private fun getPopularMovies() {
        viewModel.getPopularMovies(1).observe(this) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> loading?.show()
                    is RequestState.Success -> it.data.results.let { data ->
                        loading?.hide()
                        popularMoviesAdapter?.setMovies(data)
                        binding?.rvPopularMovie?.adapter = popularMoviesAdapter
                    }
                    is RequestState.Error -> {
                        loading?.hide()
                        alert?.errorAlert(it.message)
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}