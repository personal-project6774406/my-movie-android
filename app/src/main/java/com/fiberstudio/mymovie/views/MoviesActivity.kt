package com.fiberstudio.mymovie.views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fiberstudio.mymovie.databinding.ActivityMoviesBinding
import com.fiberstudio.mymovie.utils.adapters.MoviesAdapter
import com.fiberstudio.mymovie.utils.dialogues.AlertDialog
import com.fiberstudio.mymovie.utils.listeners.OnMovieClickListener
import com.fiberstudio.mymovie.utils.networking.RequestState
import com.fiberstudio.mymovie.viewmodels.MovieViewModel

class MoviesActivity : AppCompatActivity() {
    private var _binding: ActivityMoviesBinding? = null
    private val binding get() = _binding
    private var adapter: MoviesAdapter? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    private val viewModel: MovieViewModel by viewModels()
    private var genres: String? = null
    private var genresId: String? = null
    private var title: String? = null
    private var alert: AlertDialog? = null
    private var state = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMoviesBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        // setup dialogues
        alert = AlertDialog(this)

        state = intent.getIntExtra("STATE", 0)

        when (state) {
            0 -> {
                genres = intent.getStringExtra("GENRES")
                genresId = intent.getIntExtra("GENRES_ID", 0).toString()
                genresId?.let { viewModel.discoverMovie(it) }
            }
            1 -> {
                title = intent.getStringExtra("TITLE")
                viewModel.getUpcomingMovieList()
            }
            2 -> {
                title = intent.getStringExtra("TITLE")
                viewModel.getPopularMovieList()
            }
        }


        val getTitle = when (state) {
            0 -> "Genre: $genres"
            1 -> title
            2 -> title
            else -> ""
        }
        binding?.genre?.text = getTitle

        requestThenObserveAnychangeGenres()
        observeAnychangeMovies()
        setupRecyclerView()

        adapter?.setOnMovieClickListener(object: OnMovieClickListener {
            override fun onMovieClick(id: Int) {
                startActivity(Intent(this@MoviesActivity, DetailMovieActivity::class.java).putExtra("GENRES_ID", id))
            }
        })

        // handle back event
        binding?.back?.setOnClickListener { finish() }
    }

    private fun observeAnychangeMovies() {
        viewModel.discoverResponse.observe(this) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> showLoading()
                    is RequestState.Success -> {
                        hideLoading()
                        it.data?.results?.let { data ->
                            adapter?.differ?.submitList(data.toList())
                        }

                    }
                    is RequestState.Error -> {
                        hideLoading()
                        alert?.errorAlert(it.message)
                    }
                }
            }
        }
    }

    private fun requestThenObserveAnychangeGenres() {
        viewModel.getGenres().observe(this) {
            if (it != null) {
                when (it) {
                    is RequestState.Loading -> {}
                    is RequestState.Success -> it.data.genres?.let { data -> adapter?.setGenres(data) }
                    is RequestState.Error -> alert?.errorAlert(it.message)
                }
            }
        }
    }

    private fun setupRecyclerView() {
        adapter = MoviesAdapter()
        layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding?.apply {
            movieList.adapter = adapter
            movieList.layoutManager = layoutManager
            movieList.addOnScrollListener(scrollListener)
        }
    }

    private val scrollListener = object: RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            if (!recyclerView.canScrollVertically(1)) {
                when (state) {
                    0 -> viewModel.getUpcomingMovieList()
                    1 -> genresId?.let { viewModel.discoverMovie(it) }
                    2 -> viewModel.getPopularMovieList()
                }
            }
        }
    }

    private fun showLoading() {
        binding?.loading?.show()
    }
    private fun hideLoading() {
        binding?.loading?.hide()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}