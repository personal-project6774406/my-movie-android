package com.fiberstudio.mymovie.models.genres

import com.google.gson.annotations.SerializedName

data class Response(
    @field:SerializedName("genres")
    val genres: List<Genres>? = null
)
