package com.fiberstudio.mymovie.models.movies.list

import com.google.gson.annotations.SerializedName

data class Response(
    @field:SerializedName("page")
    val page: Int? = 0,

    @field:SerializedName("total_pages")
    val totalPages: Int? = 0,

    @field:SerializedName("results")
    val results: MutableList<Movies>,

    @field:SerializedName("total_results")
    val totalResults: Int? = 0
)
