package com.fiberstudio.mymovie.models.movies.detail

import com.google.gson.annotations.SerializedName

data class Videos(

	@field:SerializedName("results")
	val results: List<VideoItem?>? = null
)