package com.fiberstudio.mymovie.utils.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fiberstudio.mymovie.BuildConfig
import com.fiberstudio.mymovie.databinding.MovieGridItemsBinding
import com.fiberstudio.mymovie.models.genres.Genres
import com.fiberstudio.mymovie.models.movies.list.Movies
import com.fiberstudio.mymovie.utils.listeners.OnMovieClickListener

class GridMovieAdapter: RecyclerView.Adapter<GridMovieAdapter.ViewHolder>() {
    inner class ViewHolder(val binding: MovieGridItemsBinding): RecyclerView.ViewHolder(binding.root)

    private val movies = ArrayList<Movies>()
    private val genres = ArrayList<Genres>()
    private var onMovieClickListener: OnMovieClickListener? = null

    fun setGenres(list: List<Genres>) {
        this.genres.clear()
        this.genres.addAll(list)
    }

    fun setMovies(list: List<Movies>) {
        this.movies.clear()
        this.movies.addAll(list)
    }

    fun setOnMovieClickListener(onMovieClickListener: OnMovieClickListener) {
        this.onMovieClickListener = onMovieClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GridMovieAdapter.ViewHolder {
        val binding = MovieGridItemsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = 5

    override fun onBindViewHolder(holder: GridMovieAdapter.ViewHolder, position: Int) {
        with(holder) {
            with(movies[position]) {
                binding.apply {
                    Glide.with(itemView).load("${BuildConfig.IMAGE_URL}$posterPath").into(img)
                    movieName.text = originalTitle
                    language.text = originalLanguage
                    val map = genres.associate { it.id to it.name }
                    val genres = StringBuilder()

                    val genresId = ArrayList<Int>()
                    if (genreIds != null) {
                        genresId.addAll(genreIds)
                        for (data in genreIds) {
                            genres.append("${map[data]}, ")
                        }
                    }

                    genresText.text = genres.dropLast(2)

                    ratingText.text = String.format("%.1f", voteAverage).toDouble().toString()
                    ratingBar.rating = voteAverage?.div(2) ?: 0f

                    itemView.setOnClickListener { id?.let { id ->
                        onMovieClickListener?.onMovieClick(id) } }
                }
            }
        }
    }
}