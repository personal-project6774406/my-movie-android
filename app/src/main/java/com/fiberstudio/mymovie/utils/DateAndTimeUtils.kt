package com.fiberstudio.mymovie.utils

import java.text.SimpleDateFormat
import java.util.Locale

class DateAndTimeUtils {
    companion object {
        fun formatDate(inputDate: String): String {
            val inputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val outputFormat = SimpleDateFormat("MMMM d, yyyy", Locale.getDefault())

            try {
                val date = inputFormat.parse(inputDate)
                return outputFormat.format(date!!)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return ""
        }
    }
}