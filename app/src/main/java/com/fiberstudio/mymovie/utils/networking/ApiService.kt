package com.fiberstudio.mymovie.utils.networking

import com.fiberstudio.mymovie.models.movies.detail.DetailResponse
import com.fiberstudio.mymovie.models.movies.reviews.ReviewResponse
import retrofit2.Response
import com.fiberstudio.mymovie.models.genres.Response as Genres
import com.fiberstudio.mymovie.models.movies.list.Response as Movies
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    // get genres
    @GET("genre/movie/list")
    suspend fun getGenres(
        @Query("api_key") key: String?
    ): Genres

    // get movie by genre
    @GET("discover/movie")
    suspend fun discoverMovies(
        @Query("api_key") key: String?,
        @Query("with_genres") genreId: String?,
        @Query("page") page: Int?
    ): Response<Movies>

    // get detail movie
    @GET("movie/{movie_id}")
    suspend fun getDetailMovie(
        @Path("movie_id") id: Int?,
        @Query("api_key") key: String?,
        @Query("append_to_response") appendToResponse: String?
    ): DetailResponse

    // get movie's reviews
    @GET("movie/{movie_id}/reviews")
    suspend fun getMoviesReviews(
        @Path("movie_id") id: Int?,
        @Query("api_key") key: String?,
        @Query("page") page: Int?
    ): Response<ReviewResponse>

    @GET("movie/upcoming")
    suspend fun getUpcomingMovies(
        @Query("api_key") key: String?,
        @Query("page") page: Int?
    ): Movies

    @GET("movie/upcoming")
    suspend fun getUpcomingMovieList(
        @Query("api_key") key: String?,
        @Query("page") page: Int?
    ): Response<Movies>

    @GET("movie/popular")
    suspend fun getPopularMovies(
        @Query("api_key") key: String?,
        @Query("page") page: Int?
    ): Movies

    @GET("movie/popular")
    suspend fun getPopularMovieList(
        @Query("api_key") key: String?,
        @Query("page") page: Int?
    ): Response<Movies>
}