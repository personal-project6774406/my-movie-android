package com.fiberstudio.mymovie.utils.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.fiberstudio.mymovie.R
import com.fiberstudio.mymovie.databinding.ContentRvGenresBinding
import com.fiberstudio.mymovie.models.genres.Genres
import com.fiberstudio.mymovie.utils.listeners.OnGenreClickListener

class GenresAdapter(private val context: Context): RecyclerView.Adapter<GenresAdapter.ViewHolder>() {
    private val genres = ArrayList<Genres>()
    private var index = -1
    private var onGenreClickListener: OnGenreClickListener? = null
    inner class ViewHolder(val binding: ContentRvGenresBinding): RecyclerView.ViewHolder(binding.root)

    fun setGenres(list: List<Genres>) {
        this.genres.clear()
        this.genres.addAll(list)
        notifyDataSetChanged()
    }

    fun setOnGenreClickListener(onGenreClickListener: OnGenreClickListener) {
        this.onGenreClickListener = onGenreClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ContentRvGenresBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = genres.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val attrs = context.theme.obtainStyledAttributes(R.styleable.MaterialCardView)
        val backgroundColor = attrs.getColor(R.styleable.MaterialCardView_cardBackgroundColor, ContextCompat.getColor(context, R.color.transparent))
        with(holder){
            with(genres[position]){
                binding.card.setCardBackgroundColor(backgroundColor)
                binding.tvGenreName.text = name

                itemView.setOnClickListener {
                    if (id != null && name != null) onGenreClickListener?.onGenreClick(id, name)
                    index = adapterPosition
                    notifyDataSetChanged()
                }

                if (index == position) {
                    binding.card.setCardBackgroundColor(ContextCompat.getColorStateList(context, R.color.material_blue))
                    binding.tvGenreName.setTextColor(ContextCompat.getColor(context, R.color.white))
                }
            }
        }
    }
}