package com.fiberstudio.mymovie.utils.listeners

interface OnGenreClickListener {
    fun onGenreClick(id: Int, genres: String)
}