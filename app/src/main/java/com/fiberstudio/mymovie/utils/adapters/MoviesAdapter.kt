package com.fiberstudio.mymovie.utils.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fiberstudio.mymovie.BuildConfig
import com.fiberstudio.mymovie.databinding.MovieListBinding
import com.fiberstudio.mymovie.models.genres.Genres
import com.fiberstudio.mymovie.models.movies.list.Movies
import com.fiberstudio.mymovie.utils.DateAndTimeUtils
import com.fiberstudio.mymovie.utils.listeners.OnMovieClickListener

class MoviesAdapter: RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {
    private var onMovieClickListener: OnMovieClickListener? = null
    private val genres = ArrayList<Genres>()

    fun setOnMovieClickListener(onMovieClickListener: OnMovieClickListener) {
        this.onMovieClickListener = onMovieClickListener
    }

    fun setGenres(list: List<Genres>) {
        this.genres.clear()
        this.genres.addAll(list)
    }

    private val differCallback = object: DiffUtil.ItemCallback<Movies>() {
        override fun areItemsTheSame(oldItem: Movies, newItem: Movies): Boolean = oldItem == newItem
        override fun areContentsTheSame(oldItem: Movies, newItem: Movies): Boolean = oldItem == newItem
    }

    val differ = AsyncListDiffer(this, differCallback)

    inner class ViewHolder(val binding: MovieListBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = MovieListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = differ.currentList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) {
            with(differ.currentList[position]){
                binding.apply {
                    title.text = originalTitle
                    lang.text = originalLanguage
                    releaseDate.text = differ.currentList[position].releaseDate?.let {
                        DateAndTimeUtils.formatDate(it)
                    }
                    ratingText.text = String.format("%.1f", voteAverage).toDouble().toString()
                    ratingBar.rating = voteAverage?.div(2) ?: 0f // rating star = 5, max vote value = 10

                    Glide.with(itemView).load("${BuildConfig.IMAGE_URL}$posterPath").into(poster)

                    // convert list of id genre into name
                    val map = genres.associate { it.id to it.name }
                    val genres = StringBuilder()

                    val genresId = ArrayList<Int>()
                    if (genreIds != null) {
                        genresId.addAll(genreIds)
                        for (data in genreIds) {
                            genres.append("${map[data]}, ")
                        }
                    }

                    genre.text = genres.dropLast(2) // remove ,(space) after last genres

                    itemView.setOnClickListener { id?.let { id ->
                        onMovieClickListener?.onMovieClick(id) } }
                }
            }
        }
    }
}