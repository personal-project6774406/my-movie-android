package com.fiberstudio.mymovie.utils.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fiberstudio.mymovie.BuildConfig
import com.fiberstudio.mymovie.databinding.ReviewListBinding
import com.fiberstudio.mymovie.models.movies.reviews.ResultsItem

class ReviewsAdapter: RecyclerView.Adapter<ReviewsAdapter.ViewHolder>() {

    private val differCallback = object: DiffUtil.ItemCallback<ResultsItem>() {
        override fun areItemsTheSame(oldItem: ResultsItem, newItem: ResultsItem): Boolean = oldItem == newItem
        override fun areContentsTheSame(oldItem: ResultsItem, newItem: ResultsItem): Boolean = oldItem == newItem
    }

    val differ = AsyncListDiffer(this, differCallback)

    inner class ViewHolder(val binding: ReviewListBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ReviewListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = differ.currentList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) {
            with(differ.currentList[position]){
                binding.apply {
                    username.text = authorDetails?.username
                    if (authorDetails?.rating == null) {
                        ratingText.text = "0"
                        ratingBar.rating = 0f
                    } else {
                        ratingText.text = authorDetails.rating.toString()
                        ratingBar.rating = authorDetails.rating.toFloat().div(2)
                    }
                    content.text = differ.currentList[position].content
                    Glide.with(itemView).load(if (authorDetails?.avatarPath == null) "https://media.istockphoto.com/id/1300845620/vector/user-icon-flat-isolated-on-white-background-user-symbol-vector-illustration.jpg?s=612x612&w=0&k=20&c=yBeyba0hUkh14_jgv1OKqIH0CCSWU_4ckRkAoy2p73o=" else "${BuildConfig.IMAGE_URL}${authorDetails.avatarPath}").circleCrop().into(avatar)
                }
            }
        }
    }
}