package com.fiberstudio.mymovie.utils.listeners

interface OnMovieClickListener {
    fun onMovieClick(id: Int)
}